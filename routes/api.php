<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("login", [UserController::class, 'userLogin']);
Route::group(['middleware' => 'auth:api'], function(){
    Route::post("logout", [UserController::class, 'userLogout']);
    Route::get("profile", [UserController::class, 'userProfile']);
    Route::get("clients", [ClientController::class, 'clients']);
    Route::put("client", [ClientController::class, 'clientUpdate']);
    Route::post("client", [ClientController::class, 'clientInsert']);
    Route::delete("client/{id}", [ClientController::class, 'clientDelete']);
    Route::get("client/{id}/users", [ClientController::class, 'clientOwners']);
    Route::get("employees", [EmployeeController::class, 'employees']);
    Route::put("employee", [EmployeeController::class, 'employeeUpdate']);
    Route::post("employee", [EmployeeController::class, 'employeeInsert']);
    Route::delete("employee/{id}", [EmployeeController::class, 'employeeDelete']);
});