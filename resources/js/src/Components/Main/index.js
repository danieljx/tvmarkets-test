import React, { useState, useEffect } from "react";
import { Route } from "react-router-dom";
import { Container, Row } from "react-bootstrap";
import LoadingSpinner from "../../Utils/LoadingSpinner";
import Header from "../Header";
import SideBar from "../SideBar";
import Clients from "../Clients/List";
import Employees from "../Employees/List";
import Users from "../Users/List";
import Fetch from "../../Utils/Fetch";
import { profileUrl } from "../../../Api/user";
import { ToastTvmarkets } from "../../Utils/Toast";
import "./style.css";

const Main = () => {
    const [sessionLoaded, setSessionLoaded] = useState(false);
    const [profile, setProfile] = useState(null);

    const fetchProfile = () => {
        Fetch({
            url: profileUrl,
            method: "get",
        }).then((dataResp) => {
            if (dataResp.status === 200) {
                setProfile(dataResp.user);
                setSessionLoaded(true);
            } else {
                ToastTvmarkets.error(dataResp.message);
            }
        });
    };

    const isAdmin = () => {
        let BoK = false;
        if (profile) {
            if (profile.roles.length > 0) {
                for (let r = 0; r < profile.roles.length; r += 1) {
                    if (!BoK) {
                        BoK = profile.roles[r].admin === 1;
                    }
                }
            }
        }
        return BoK;
    };

    useEffect(() => {
        fetchProfile();
    }, []);

    return (
        <>
            {sessionLoaded ? (
                <>
                    <Header profile={profile} />
                    <Container fluid>
                        <Row>
                            <SideBar isAdmin={isAdmin} />
                            <main
                                role="main"
                                className="col-md-9 ml-sm-auto col-lg-10 px-md-4"
                            >
                                <Route exact path="/clients">
                                    <Clients isAdmin={isAdmin} />
                                </Route>
                                <Route
                                    exact
                                    path="/employees"
                                    component={Employees}
                                />
                                <Route exact path="/users" component={Users} />
                            </main>
                        </Row>
                    </Container>
                </>
            ) : (
                <LoadingSpinner />
            )}
        </>
    );
};

export default Main;
