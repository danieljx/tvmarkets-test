import React, { useRef, useState, useEffect } from "react";
import {
    Modal,
    Button,
    Form,
    Overlay,
    Tooltip,
    Row,
    Col,
} from "react-bootstrap";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import { useForm } from "react-hook-form";
import { clientUrl } from "../../../Api/clients";
import Fetch from "../../Utils/Fetch";
import { ToastTvmarkets } from "../../Utils/Toast";

const Edit = (props) => {
    const { isAdmin, client } = props;
    const [clientLoaded, setClientLoaded] = useState(true);
    const [userOwnerClient, setUserOwnerClientLoaded] = useState(false);
    const [users, setUsers] = useState([]);
    const [usersSelected, setUsersSelected] = useState([]);
    const [usersTemp, setUsersTemp] = useState([]);
    const { register, handleSubmit, errors } = useForm();
    let refName = useRef(null);
    let refLastName = useRef(null);
    let refEmail = useRef(null);
    let refPhone = useRef(null);
    const animatedComponents = makeAnimated();

    const onSubmit = (data) => {
        data.id = client ? client.id : null;
        data.third_relations = usersTemp;
        fetchClient(data);
    };

    const fetchClient = (data) => {
        setClientLoaded(false);
        Fetch({
            url: clientUrl,
            method: client ? "put" : "post",
            data: JSON.stringify(data),
        }).then((dataResp) => {
            if (dataResp.status === 200) {
                ToastTvmarkets.success(dataResp.message);
                props.onHide(true);
            } else {
                ToastTvmarkets.error(dataResp.message);
            }
            setClientLoaded(true);
        });
    };

    const getValues = (users) => {
        const dataUser = [];
        if (users.length > 0) {
            for (let u = 0; u < users.length; u += 1) {
                dataUser.push({
                    value: users[u].id,
                    label: `${users[u].name} ${users[u].last_name}`,
                });
            }
        }
        return dataUser;
    };

    const fetchUserOwnerClient = (data) => {
        setUserOwnerClientLoaded(false);
        Fetch({
            url: `${clientUrl}/${client.id}/users`,
            method: "get",
            data: JSON.stringify(data),
        }).then((dataResp) => {
            if (dataResp.status === 200) {
                setUsers(getValues(dataResp.users));
                setUsersSelected(getValues(dataResp.usersSelected));
                setUsersTemp(getValues(dataResp.usersSelected));
            } else {
                ToastTvmarkets.error(dataResp.message);
            }
            setUserOwnerClientLoaded(true);
        });
    };

    const onUserSelected = (users) => {
        setUsersTemp(users);
    };

    useEffect(() => {
        setUserOwnerClientLoaded(false);
        setUsers([]);
        setUsersSelected([]);
        if (isAdmin() && client) {
            fetchUserOwnerClient();
        }
    }, [client]);

    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {client && (
                        <strong>{`${client.name} ${client.last_name}`}</strong>
                    )}
                    {client === null && <strong>Cliente Nuevo</strong>}
                </Modal.Title>
            </Modal.Header>
            <Form noValidate onSubmit={handleSubmit(onSubmit)}>
                <Modal.Body>
                    <Row>
                        <Col>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>First Names:</Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    name="name"
                                    placeholder="First Name"
                                    autoComplete="off"
                                    defaultValue={client ? client.name : ""}
                                    disabled={!clientLoaded}
                                    ref={(ref) => {
                                        refName.current = ref;
                                        register(ref, {
                                            required: {
                                                value: true,
                                                message:
                                                    "Valid First Name is required.",
                                            },
                                            minLength: {
                                                value: 2,
                                                message: "Must be 2 chars.",
                                            },
                                        });
                                    }}
                                />
                                <Overlay
                                    target={refName.current}
                                    show={!!errors.name}
                                    placement="left"
                                >
                                    {(props) => (
                                        <Tooltip {...props}>
                                            {errors.name
                                                ? errors.name.message
                                                : ""}
                                        </Tooltip>
                                    )}
                                </Overlay>
                            </Form.Group>
                            <Form.Group controlId="formBasicLastName">
                                <Form.Label>Last Names:</Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    name="last_name"
                                    placeholder="Last Name"
                                    autoComplete="off"
                                    defaultValue={
                                        client ? client.last_name : ""
                                    }
                                    disabled={!clientLoaded}
                                    ref={(ref) => {
                                        refLastName.current = ref;
                                        register(ref, {
                                            required: {
                                                value: true,
                                                message:
                                                    "Valid Last Name is required.",
                                            },
                                            minLength: {
                                                value: 2,
                                                message: "Must be 2 chars.",
                                            },
                                        });
                                    }}
                                />
                                <Overlay
                                    target={refLastName.current}
                                    show={!!errors.last_name}
                                    placement="left"
                                >
                                    {(props) => (
                                        <Tooltip {...props}>
                                            {errors.last_name
                                                ? errors.last_name.message
                                                : ""}
                                        </Tooltip>
                                    )}
                                </Overlay>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email:</Form.Label>
                                <Form.Control
                                    required
                                    type="email"
                                    name="email"
                                    placeholder="email@email.com"
                                    autoComplete="off"
                                    defaultValue={client ? client.email : ""}
                                    disabled={!clientLoaded}
                                    ref={(ref) => {
                                        refEmail.current = ref;
                                        register(ref, {
                                            required: {
                                                value: true,
                                                message:
                                                    "Valid email is required.",
                                            },
                                            validate: (value) =>
                                                [
                                                    /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                                ].every((pattern) =>
                                                    pattern.test(value)
                                                ) ||
                                                "Please enter a valid email.",
                                        });
                                    }}
                                />
                                <Overlay
                                    target={refEmail.current}
                                    show={!!errors.email}
                                    placement="right"
                                >
                                    {(props) => (
                                        <Tooltip {...props}>
                                            {errors.email
                                                ? errors.email.message
                                                : ""}
                                        </Tooltip>
                                    )}
                                </Overlay>
                            </Form.Group>
                            <Form.Group controlId="formBasicPhone">
                                <Form.Label>Phone:</Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    name="phone"
                                    placeholder="+1 00000000"
                                    autoComplete="off"
                                    defaultValue={client ? client.phone : ""}
                                    disabled={!clientLoaded}
                                    ref={(ref) => {
                                        refPhone.current = ref;
                                        register(ref, {
                                            required: {
                                                value: true,
                                                message:
                                                    "Valid phone is required.",
                                            },
                                            minLength: {
                                                value: 2,
                                                message: "Must be 2 chars.",
                                            },
                                        });
                                    }}
                                />
                                <Overlay
                                    target={refPhone.current}
                                    show={!!errors.phone}
                                    placement="right"
                                >
                                    {(props) => (
                                        <Tooltip {...props}>
                                            {errors.phone
                                                ? errors.phone.message
                                                : ""}
                                        </Tooltip>
                                    )}
                                </Overlay>
                            </Form.Group>
                        </Col>
                    </Row>
                    {isAdmin() && userOwnerClient && (
                        <Row>
                            <Col>
                                <Form.Group controlId="formBasicPhone">
                                    <Form.Label>Owner:</Form.Label>
                                    <Select
                                        closeMenuOnSelect={false}
                                        components={animatedComponents}
                                        defaultValue={usersSelected}
                                        isMulti
                                        options={users}
                                        onChange={onUserSelected}
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                    )}
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        variant="success"
                        type="submit"
                        disabled={!clientLoaded}
                    >
                        Save
                    </Button>
                    <Button
                        variant="danger"
                        onClick={() => props.onHide(false)}
                        disabled={!clientLoaded}
                    >
                        Cancel
                    </Button>
                </Modal.Footer>
            </Form>
        </Modal>
    );
};

export default Edit;
