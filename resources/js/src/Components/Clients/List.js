import React, { useState, useEffect } from "react";
import { Container, Card, Table, ButtonGroup, Button } from "react-bootstrap";
import LoadingSpinner from "../../Utils/LoadingSpinner";
import { clientsUrl, clientUrl } from "../../../Api/clients";
import Fetch from "../../Utils/Fetch";
import { ToastTvmarkets } from "../../Utils/Toast";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faPlus, faTrash } from "@fortawesome/free-solid-svg-icons";
import Edit from "./Edit";

const List = (props) => {
    const { isAdmin } = props;
    const [clientsLoaded, setClientsLoaded] = useState(false);
    const [clients, setClients] = useState([]);
    const [client, setClient] = useState(null);
    const [modalShow, setModalShow] = useState(false);

    const fetchClients = () => {
        Fetch({
            url: clientsUrl,
            method: "get",
        }).then((dataResp) => {
            if (dataResp.status === 200) {
                setClients(dataResp.clients);
            } else {
                ToastTvmarkets.error(dataResp.message);
            }
            setClientsLoaded(true);
        });
    };

    const onEdit = (client) => {
        setClient(client);
        setModalShow(true);
    };

    const onDelete = (client) => {
        fetchDeleteClient(client.id);
    };

    const fetchDeleteClient = (id) => {
        Fetch({
            url: `${clientUrl}/${id}`,
            method: "delete",
        }).then((dataResp) => {
            if (dataResp.status === 200) {
                ToastTvmarkets.success(dataResp.message);
                fetchClients();
            } else {
                ToastTvmarkets.error(dataResp.message);
            }
            setClientsLoaded(true);
        });
    };
    const onAdd = (e) => {
        e.preventDefault();
        setClient(null);
        setModalShow(true);
        return false;
    };

    const onHide = (refresh) => {
        setModalShow(false);
        setClient(null);
        if (refresh) {
            fetchClients();
        }
    };

    useEffect(() => {
        fetchClients();
    }, []);

    return (
        <Container fluid className="list">
            <Edit
                show={modalShow}
                onHide={(refresh) => onHide(refresh)}
                client={client}
                isAdmin={isAdmin}
            />
            <Card className="bg-dark text-white">
                <Card.Header>
                    Clients{" "}
                    <Button variant="primary" onClick={onAdd} size="sm">
                        <FontAwesomeIcon icon={faPlus} />
                    </Button>
                </Card.Header>
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {!clientsLoaded && <LoadingSpinner height="5rem" />}
                        {clients.map((client, i) => (
                            <tr
                                key={`client-${i}-${client.id}`}
                                id={`client-${client.id}`}
                            >
                                <td className="i">{i + 1}</td>
                                <td className="name">{client.name}</td>
                                <td className="lastName">{client.last_name}</td>
                                <td className="phone">{client.phone}</td>
                                <td className="email">{client.email}</td>
                                <td className="type">{client.type}</td>
                                <td className="action">
                                    <ButtonGroup size="sm">
                                        <Button
                                            variant="success"
                                            onClick={() => onEdit(client)}
                                        >
                                            <FontAwesomeIcon icon={faEdit} />
                                        </Button>
                                        <Button
                                            variant="danger"
                                            onClick={() => onDelete(client)}
                                        >
                                            <FontAwesomeIcon icon={faTrash} />
                                        </Button>
                                    </ButtonGroup>
                                </td>
                            </tr>
                        ))}
                        {clients.length === 0 && clientsLoaded && (
                            <tr>
                                <td colSpan="7">No data</td>
                            </tr>
                        )}
                    </tbody>
                </Table>
            </Card>
        </Container>
    );
};

export default List;
