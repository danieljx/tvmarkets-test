import React, { useState } from "react";
import { Redirect, Link } from "react-router-dom";
import { Navbar, Button } from "react-bootstrap";
import Fetch from "../../Utils/Fetch";
import { logoutUrl } from "../../../Api/user";
import { ToastTvmarkets } from "../../Utils/Toast";
import Session from "../../Utils/Session";

const Header = (props) => {
    const { profile } = props;
    const [loggedIn, setLoggedIn] = useState(true);

    const fetchLogout = () => {
        Fetch({
            url: logoutUrl,
            method: "post",
        }).then((dataResp) => {
            if (dataResp.status === 200) {
                Session.removeToken();
                setLoggedIn(false);
            } else {
                ToastTvmarkets.error(dataResp.message);
            }
        });
    };

    const onLogout = (e) => {
        e.preventDefault();
        fetchLogout();
        return false;
    };

    return !loggedIn ? (
        <Redirect to="/signin" />
    ) : (
        <Navbar
            bg="dark"
            variant="dark"
            className="sticky-top flex-md-nowrap shadow p-0"
        >
            <Link to="/" className="col-md-3 col-lg-2 mr-0 px-3 navbar-brand">
                tvmarkets - test
            </Link>
            <Navbar.Toggle />
            <Navbar.Collapse className="justify-content-end">
                <Navbar.Text>
                    {profile && (
                        <a href="#login">Signed in as: {profile.alias}</a>
                    )}
                </Navbar.Text>
                {profile && (
                    <Button
                        variant="outline-info"
                        className="ml-2 mr-2"
                        onClick={onLogout}
                    >
                        Logout
                    </Button>
                )}
            </Navbar.Collapse>
        </Navbar>
    );
};

export default Header;
