import React from "react";
import { Link, withRouter } from "react-router-dom";
import { Nav } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faUserFriends,
    faUsers,
    faUsersCog,
} from "@fortawesome/free-solid-svg-icons";

const SideBar = (props) => {
    const { location, isAdmin } = props;
    return (
        <Nav
            id="sidebarMenu"
            className="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse"
        >
            <div className="sidebar-sticky pt-3">
                <ul className="nav flex-column">
                    <li className="nav-item">
                        <Link
                            to="/clients"
                            className={`nav-link${
                                location.pathname.indexOf("/clients") !== -1 &&
                                location.pathname !== "/"
                                    ? " active"
                                    : ""
                            }`}
                        >
                            <FontAwesomeIcon icon={faUsers} />
                            Clients
                        </Link>
                    </li>
                    {isAdmin() && (
                        <>
                            <li className="nav-item">
                                <Link
                                    to="/employees"
                                    className={`nav-link${
                                        location.pathname.indexOf(
                                            "/employees"
                                        ) !== -1 && location.pathname !== "/"
                                            ? " active"
                                            : ""
                                    }`}
                                >
                                    <FontAwesomeIcon icon={faUserFriends} alt />
                                    Employees
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link
                                    to="/users"
                                    className={`nav-link${
                                        location.pathname.indexOf("/users") !==
                                            -1 && location.pathname !== "/"
                                            ? " active"
                                            : ""
                                    }`}
                                >
                                    <FontAwesomeIcon icon={faUsersCog} />
                                    Users
                                </Link>
                            </li>
                        </>
                    )}
                </ul>
            </div>
        </Nav>
    );
};

export default withRouter(SideBar);
