import React, { useState, useEffect } from "react";
import { Container, Card, Table, ButtonGroup, Button } from "react-bootstrap";
import LoadingSpinner from "../../Utils/LoadingSpinner";
import { employeesUrl, employeeUrl } from "../../../Api/employees";
import Fetch from "../../Utils/Fetch";
import { ToastTvmarkets } from "../../Utils/Toast";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faPlus, faTrash } from "@fortawesome/free-solid-svg-icons";
import Edit from "./Edit";

const List = (props) => {
    const { isAdmin } = props;
    const [employeesLoaded, setEmployeesLoaded] = useState(false);
    const [employees, setEmployees] = useState([]);
    const [employee, setEmployee] = useState(null);
    const [modalShow, setModalShow] = useState(false);

    const fetchEmployees = () => {
        Fetch({
            url: employeesUrl,
            method: "get",
        }).then((dataResp) => {
            if (dataResp.status === 200) {
                setEmployees(dataResp.employees);
            } else {
                ToastTvmarkets.error(dataResp.message);
            }
            setEmployeesLoaded(true);
        });
    };

    const onEdit = (employee) => {
        setEmployee(employee);
        setModalShow(true);
    };

    const onDelete = (employee) => {
        fetchDeleteEmployee(employee.id);
    };

    const fetchDeleteEmployee = (id) => {
        Fetch({
            url: `${employeeUrl}/${id}`,
            method: "delete",
        }).then((dataResp) => {
            if (dataResp.status === 200) {
                ToastTvmarkets.success(dataResp.message);
                fetchEmployees();
            } else {
                ToastTvmarkets.error(dataResp.message);
            }
        });
    };
    const onAdd = (e) => {
        e.preventDefault();
        setEmployee(null);
        setModalShow(true);
        return false;
    };

    const onHide = (refresh) => {
        setModalShow(false);
        setEmployee(null);
        if (refresh) {
            fetchEmployees();
        }
    };

    useEffect(() => {
        fetchEmployees();
    }, []);

    return (
        <Container fluid className="list">
            <Edit
                show={modalShow}
                onHide={(refresh) => onHide(refresh)}
                employee={employee}
                isAdmin={isAdmin}
            />
            <Card className="bg-dark text-white">
                <Card.Header>
                    Employees{" "}
                    <Button variant="primary" onClick={onAdd} size="sm">
                        <FontAwesomeIcon icon={faPlus} />
                    </Button>
                </Card.Header>
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {!employeesLoaded && <LoadingSpinner height="5rem" />}
                        {employees.map((employee, i) => (
                            <tr
                                key={`employee-${i}-${employee.id}`}
                                id={`employee-${employee.id}`}
                            >
                                <td className="i">{i + 1}</td>
                                <td className="name">{employee.name}</td>
                                <td className="lastName">
                                    {employee.last_name}
                                </td>
                                <td className="phone">{employee.phone}</td>
                                <td className="email">{employee.email}</td>
                                <td className="type">{employee.type}</td>
                                <td className="action">
                                    <ButtonGroup size="sm">
                                        <Button
                                            variant="success"
                                            onClick={() => onEdit(employee)}
                                        >
                                            <FontAwesomeIcon icon={faEdit} />
                                        </Button>
                                        <Button
                                            variant="danger"
                                            onClick={() => onDelete(employee)}
                                        >
                                            <FontAwesomeIcon icon={faTrash} />
                                        </Button>
                                    </ButtonGroup>
                                </td>
                            </tr>
                        ))}
                        {employees.length === 0 && employeesLoaded && (
                            <tr>
                                <td colSpan="7">No data</td>
                            </tr>
                        )}
                    </tbody>
                </Table>
            </Card>
        </Container>
    );
};

export default List;
