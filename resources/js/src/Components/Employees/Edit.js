import React, { useRef, useState, useEffect } from "react";
import {
    Modal,
    Button,
    Form,
    Overlay,
    Tooltip,
    Row,
    Col,
} from "react-bootstrap";
import { useForm } from "react-hook-form";
import { employeeUrl } from "../../../Api/employees";
import Fetch from "../../Utils/Fetch";
import { ToastTvmarkets } from "../../Utils/Toast";

const Edit = (props) => {
    const { employee } = props;
    const [employeeLoaded, setEmployeeLoaded] = useState(true);
    const { register, handleSubmit, errors } = useForm();
    let refName = useRef(null);
    let refLastName = useRef(null);
    let refEmail = useRef(null);
    let refPhone = useRef(null);

    const onSubmit = (data) => {
        data.id = employee ? employee.id : null;
        fetchEmployee(data);
    };

    const fetchEmployee = (data) => {
        setEmployeeLoaded(false);
        Fetch({
            url: employeeUrl,
            method: employee ? "put" : "post",
            data: JSON.stringify(data),
        }).then((dataResp) => {
            if (dataResp.status === 200) {
                ToastTvmarkets.success(dataResp.message);
                props.onHide(true);
            } else {
                ToastTvmarkets.error(dataResp.message);
            }
            setEmployeeLoaded(true);
        });
    };

    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {employee && (
                        <strong>{`${employee.name} ${employee.last_name}`}</strong>
                    )}
                    {employee === null && <strong>Employeee Nuevo</strong>}
                </Modal.Title>
            </Modal.Header>
            <Form noValidate onSubmit={handleSubmit(onSubmit)}>
                <Modal.Body>
                    <Row>
                        <Col>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>First Names:</Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    name="name"
                                    placeholder="First Name"
                                    autoComplete="off"
                                    defaultValue={employee ? employee.name : ""}
                                    disabled={!employeeLoaded}
                                    ref={(ref) => {
                                        refName.current = ref;
                                        register(ref, {
                                            required: {
                                                value: true,
                                                message:
                                                    "Valid First Name is required.",
                                            },
                                            minLength: {
                                                value: 2,
                                                message: "Must be 2 chars.",
                                            },
                                        });
                                    }}
                                />
                                <Overlay
                                    target={refName.current}
                                    show={!!errors.name}
                                    placement="left"
                                >
                                    {(props) => (
                                        <Tooltip {...props}>
                                            {errors.name
                                                ? errors.name.message
                                                : ""}
                                        </Tooltip>
                                    )}
                                </Overlay>
                            </Form.Group>
                            <Form.Group controlId="formBasicLastName">
                                <Form.Label>Last Names:</Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    name="last_name"
                                    placeholder="Last Name"
                                    autoComplete="off"
                                    defaultValue={
                                        employee ? employee.last_name : ""
                                    }
                                    disabled={!employeeLoaded}
                                    ref={(ref) => {
                                        refLastName.current = ref;
                                        register(ref, {
                                            required: {
                                                value: true,
                                                message:
                                                    "Valid Last Name is required.",
                                            },
                                            minLength: {
                                                value: 2,
                                                message: "Must be 2 chars.",
                                            },
                                        });
                                    }}
                                />
                                <Overlay
                                    target={refLastName.current}
                                    show={!!errors.last_name}
                                    placement="left"
                                >
                                    {(props) => (
                                        <Tooltip {...props}>
                                            {errors.last_name
                                                ? errors.last_name.message
                                                : ""}
                                        </Tooltip>
                                    )}
                                </Overlay>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email:</Form.Label>
                                <Form.Control
                                    required
                                    type="email"
                                    name="email"
                                    placeholder="email@email.com"
                                    autoComplete="off"
                                    defaultValue={
                                        employee ? employee.email : ""
                                    }
                                    disabled={!employeeLoaded}
                                    ref={(ref) => {
                                        refEmail.current = ref;
                                        register(ref, {
                                            required: {
                                                value: true,
                                                message:
                                                    "Valid email is required.",
                                            },
                                            validate: (value) =>
                                                [
                                                    /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                                ].every((pattern) =>
                                                    pattern.test(value)
                                                ) ||
                                                "Please enter a valid email.",
                                        });
                                    }}
                                />
                                <Overlay
                                    target={refEmail.current}
                                    show={!!errors.email}
                                    placement="right"
                                >
                                    {(props) => (
                                        <Tooltip {...props}>
                                            {errors.email
                                                ? errors.email.message
                                                : ""}
                                        </Tooltip>
                                    )}
                                </Overlay>
                            </Form.Group>
                            <Form.Group controlId="formBasicPhone">
                                <Form.Label>Phone:</Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    name="phone"
                                    placeholder="+1 00000000"
                                    autoComplete="off"
                                    defaultValue={
                                        employee ? employee.phone : ""
                                    }
                                    disabled={!employeeLoaded}
                                    ref={(ref) => {
                                        refPhone.current = ref;
                                        register(ref, {
                                            required: {
                                                value: true,
                                                message:
                                                    "Valid phone is required.",
                                            },
                                            minLength: {
                                                value: 2,
                                                message: "Must be 2 chars.",
                                            },
                                        });
                                    }}
                                />
                                <Overlay
                                    target={refPhone.current}
                                    show={!!errors.phone}
                                    placement="right"
                                >
                                    {(props) => (
                                        <Tooltip {...props}>
                                            {errors.phone
                                                ? errors.phone.message
                                                : ""}
                                        </Tooltip>
                                    )}
                                </Overlay>
                            </Form.Group>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        variant="success"
                        type="submit"
                        disabled={!employeeLoaded}
                    >
                        Save
                    </Button>
                    <Button
                        variant="danger"
                        onClick={() => props.onHide(false)}
                        disabled={!employeeLoaded}
                    >
                        Cancel
                    </Button>
                </Modal.Footer>
            </Form>
        </Modal>
    );
};

export default Edit;
