import React, { useRef, useState } from "react";
import { Redirect } from "react-router-dom";
import { Container, Form, Button, Overlay, Tooltip } from "react-bootstrap";
import { useForm } from "react-hook-form";
import Session from "../../Utils/Session";
import Fetch from "../../Utils/Fetch";
import { signinUrl } from "../../../Api/signin";
import { ToastTvmarkets } from "../../Utils/Toast";
import "./style.css";

const Signin = () => {
    const { register, handleSubmit, errors } = useForm();
    const [loggedIn, setLoggedIn] = useState(false);
    let refUserName = useRef(null);
    let refPassword = useRef(null);

    const onSubmit = (data) => {
        fetchSignIn(data);
    };

    const fetchSignIn = (data) => {
        Fetch({
            url: signinUrl,
            method: "post",
            data: JSON.stringify(data),
        }).then((dataResp) => {
            if (dataResp.status === 200) {
                Session.setToken(dataResp.access_token);
                ToastTvmarkets.success("Login realizado con éxito");
                setLoggedIn(true);
            } else {
                ToastTvmarkets.error(dataResp.message);
            }
        });
    };

    return loggedIn ? (
        <Redirect to="/" />
    ) : (
        <Container className="signin">
            <Form noValidate onSubmit={handleSubmit(onSubmit)}>
                <Form.Group controlId="formBasicEmail">
                    <Form.Control
                        required
                        type="text"
                        name="username"
                        placeholder="User Name"
                        autoComplete="off"
                        ref={(ref) => {
                            refUserName.current = ref;
                            register(ref, {
                                required: {
                                    value: true,
                                    message: "Valid username is required.",
                                },
                                minLength: {
                                    value: 2,
                                    message: "Must be 2 chars.",
                                },
                            });
                        }}
                    />
                    <Overlay
                        target={refUserName.current}
                        show={!!errors.username}
                        placement="right"
                    >
                        {(props) => (
                            <Tooltip id="overlay-username" {...props}>
                                {errors.username ? errors.username.message : ""}
                            </Tooltip>
                        )}
                    </Overlay>
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Control
                        required
                        type="password"
                        name="password"
                        placeholder="Password"
                        ref={(ref) => {
                            refPassword.current = ref;
                            register(ref, {
                                required: {
                                    value: true,
                                    message: "Valid password is required.",
                                },
                            });
                        }}
                        className={errors.password ? "invalid" : ""}
                    />
                    <Overlay
                        target={refPassword.current}
                        show={!!errors.password}
                        placement="right"
                    >
                        {(props) => (
                            <Tooltip id="overlay-password" {...props}>
                                {errors.password ? errors.password.message : ""}
                            </Tooltip>
                        )}
                    </Overlay>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Login
                </Button>
            </Form>
        </Container>
    );
};

export default Signin;
