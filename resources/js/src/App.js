import React from "react";
import { Switch, Route } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import Signin from "./Components/Signin";
import Main from "./Components/Main";
import Page404 from "./Components/404";
import ProtectedRoute from "./Utils/ProtectedRoute";
import { ToastTvmarketsContainer } from "./Utils/Toast";

const App = () => {
    return (
        <>
            <Switch>
                <Route exact path="/signin" component={Signin} />
                <ProtectedRoute path="/" component={Main} />
                <Route component={Page404} />
            </Switch>
            <ToastTvmarketsContainer />
        </>
    );
};

export default App;
