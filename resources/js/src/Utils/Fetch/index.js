import { ToastTvmarkets } from "../Toast";
import Session from "../Session";

export default ({ url, method, data, authorization, contentType }) => {
    const headers = {};
    headers.Accept = "application/json";
    if (authorization) {
        headers.Authorization = authorization;
    } else if (Session.isAuthenticated()) {
        headers.Authorization = `Bearer ${Session.getToken()}`;
    }
    if (contentType) {
        headers["Content-Type"] = contentType;
    } else if (!(data instanceof FormData)) {
        headers["Content-Type"] = "application/json";
    }
    return fetch(url, {
        method,
        headers,
        body: data || null,
    })
        .then((response) => {
            if (response.status === 500) {
                return response.json().then((respJson) => {
                    ToastTvmarkets.error(respJson.message);
                    throw Error(respJson.message);
                });
            } else if (response.status === 401) {
                Session.removeToken();
                ToastTvmarkets.warning("la sesión ha expirado");
                window.history.pushState(false, "", "/signin");
            }
            return response.json();
        })
        .catch((error) => ToastTvmarkets.error(error.toString()));
};
