import React from "react";
import Spinner from "react-bootstrap/Spinner";

const LoadingSpinner = ({ height }) => {
    return (
        <div
            style={{
                height: height ? height : "100vh",
                position: "relative",
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "column",
            }}
        >
            <Spinner
                as="span"
                animation="grow"
                role="status"
                aria-hidden="true"
            />
        </div>
    );
};
export default LoadingSpinner;
