class Session {
    constructor() {
        this.token = localStorage.getItem("token");
    }

    getToken() {
        return this.token;
    }

    setToken(token) {
        this.token = token;
        localStorage.setItem("token", token);
    }

    isAuthenticated() {
        return (
            this.token !== null && this.token !== undefined && this.token !== ""
        );
    }

    async removeToken() {
        await localStorage.removeItem("token");
    }
}

export default new Session();
