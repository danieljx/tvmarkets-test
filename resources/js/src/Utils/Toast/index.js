import React from "react";
import { toast, ToastContainer } from "react-toastify";
import { Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faCheckCircle,
    faInfo,
    faExclamationCircle,
    faTimesCircle,
} from "@fortawesome/free-solid-svg-icons";

const containerIds = "toast-tvmarkets";
const classNames = "toast-tvmarkets";

export const ToastTvmarketsContainer = (props) => {
    return (
        <ToastContainer
            enableMultiContainer
            containerId={containerIds}
            toastClassName={classNames}
            position="bottom-right"
            autoClose={5000}
            newestOnTop
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable={false}
            pauseOnHover
            {...props}
        />
    );
};

const ToastTvmarketsBody = ({ text, type }) => (
    <Row>
        <Col className="col-3">
            {(function () {
                switch (type) {
                    case toast.TYPE.SUCCESS:
                        return <FontAwesomeIcon icon={faCheckCircle} />;
                    case toast.TYPE.INFO:
                        return <FontAwesomeIcon icon={faInfo} />;
                    case toast.TYPE.WARNING:
                        return <FontAwesomeIcon icon={faExclamationCircle} />;
                    case toast.TYPE.ERROR:
                        return <FontAwesomeIcon icon={faTimesCircle} />;
                    default:
                        return null;
                }
            })()}
        </Col>
        <Col className="col-9">
            <span>{text}</span>
        </Col>
    </Row>
);

export const ToastTvmarkets = {
    success: (text, opt = {}) => {
        opt.containerId = containerIds;
        opt.type = toast.TYPE.SUCCESS;
        toast(<ToastTvmarketsBody text={text} type={opt.type} />, opt);
    },
    info: (text, opt = {}) => {
        opt.containerId = containerIds;
        opt.type = toast.TYPE.INFO;
        toast(<ToastTvmarketsBody text={text} type={opt.type} />, opt);
    },
    warn: (text, opt = {}) => {
        opt.containerId = containerIds;
        opt.type = toast.TYPE.WARNING;
        toast(<ToastTvmarketsBody text={text} type={opt.type} />, opt);
    },
    error: (text, opt = {}) => {
        opt.containerId = containerIds;
        opt.type = toast.TYPE.ERROR;
        toast(<ToastTvmarketsBody text={text} type={opt.type} />, opt);
    },
};
