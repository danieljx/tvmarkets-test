import { hostUrl } from "./host";
export const employeesUrl = `${hostUrl}/employees`;
export const employeeUrl = `${hostUrl}/employee`;
