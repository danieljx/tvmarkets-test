import { hostUrl } from "./host";
export const profileUrl = `${hostUrl}/profile`;
export const logoutUrl = `${hostUrl}/logout`;
