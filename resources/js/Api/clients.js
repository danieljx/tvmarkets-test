import { hostUrl } from "./host";
export const clientsUrl = `${hostUrl}/clients`;
export const clientUrl = `${hostUrl}/client`;
