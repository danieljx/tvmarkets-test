<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role;
use App\Models\User;

class RoleRelation extends Model
{
    use HasFactory;

    protected $table = 'roles_relations';

    protected $fillable = [
        'role_id',
        'user_id',
    ];

    public function roles()
    {
        return $this->belongsTo(Role::class, "fk_table1_roles1", "role_id");
    }

    public function users()
    {
        return $this->belongsTo(User::class, "fk_table1_users1", "user_id");
    }

    public $timestamps = false;
}
