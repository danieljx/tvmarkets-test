<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Third extends Model
{
    use HasFactory;

    protected $table = 'third';

    protected $fillable = [
        'name',
        'last_name',
        'phone',
        'email',
        'third_type',
    ];

    public $timestamps = false;
}
