<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\RoleRelation;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'alias',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function roleRelation()
    {
        return $this->hasMany(RoleRelation::class);
    }

    public $timestamps = false;


    public static function getRoles($id)
    {
        return (new static)->select('roles.*')
            ->join('roles_relations', 'roles_relations.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'roles_relations.role_id')
            ->where('users.id', $id);
    }

}
