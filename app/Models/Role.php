<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\RoleRelation;

class Role extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'admin',
        'employee',
    ];

    public function roleRelation()
    {
        return $this->hasMany(RoleRelation::class);
    }

    public $timestamps = false;
}
