<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Third;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{

    public function employees(Request $request) {
        $user = auth()->user();
        $employees = Third::where([
            ["id", "!=", 1],
            ["id", "!=", $user["id"]],
            ["third.third_type", "=", 1],
        ])->get();
        return response()->json(["status" => 200, "employees" => $employees], 200);
    }

    public function employeeUpdate(Request $request) {
        $employee = Third::where([
            ["id", "=", $request->id],
        ])->first();
        if ($employee != null) {
            $employee->name = $request->name;
            $employee->last_name = $request->last_name;
            $employee->phone = $request->phone;
            $employee->email = $request->email;
            $employee->save();
            return response()->json(["status" => 200, "employee" => $employee, "message" => "Employee: " . $employee->name . " " . $employee->last_name . ". update"], 200);
        } else {
        return response()->json(["status" => 404, "employee" => $employee, "message" => "Employee not found"], 404);
        }
    }

    public function employeeInsert(Request $request) {
        $employeeExist = Third::where([
            ["email", "=", $request->email],
        ])->first();

        if ($employeeExist == null) {
            $employee = Third::create(["name" => $request->name, "last_name" => $request->last_name, "phone" => $request->phone, "email" => $request->email, "third_type" => 1]);
            if ($employee) {
                return response()->json(["status" => 200, "employee" => $employee, "message" => "Employee: " . $request->name . " " . $request->last_name . ". add"], 200);
            } else {
                return response()->json(["status" => 204, "employee" => $employee, "message" => "Employee not insert"], 204);
            }
        }
        return response()->json(["status" => 204, "employee" => null, "message" => "Employee email: " . $request->email . " exist"], 204);
    }

    public function employeeDelete($id) {
        $employee = Third::find($id);
        if ($employee != null) {
            DB::table('third_relations')->where(['third_father' => $employee["id"]])->delete();
            $employee->delete();
            return response()->json(["status" => 200, "employee" => $employee, "message" => "Employee: " . $employee->name . " " . $employee->last_name . ". delete"], 200);
        } else {
        return response()->json(["status" => 404, "employee" => $employee, "message" => "Employee not found"], 404);
        }
    }

}
