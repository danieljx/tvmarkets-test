<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Third;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{

    public function clients(Request $request) {
        $user = auth()->user();
        if ($user["id"] !== 1) {
            $clients = User::select('third.*')
                ->join('third_relations', 'third_relations.third_father', '=', "users.third_id")
                ->join('third', 'third.id', '=', "third_relations.third_id")
                ->where([
                    ["users.id", "!=", 1],
                    ["users.id", "=", $user["id"]],
                    ["third.third_type", "=", 2],
                ])->get();
        } else {
            $clients = Third::where([
                ["id", "!=", $user["id"]],
                ["third.third_type", "=", 2],
            ])->get();
        }
        return response()->json(["status" => 200, "clients" => $clients], 200);
    }

    public function clientUpdate(Request $request) {
        $client = Third::where([
            ["id", "=", $request->id],
        ])->first();
        if ($client != null) {
            $client->name = $request->name;
            $client->last_name = $request->last_name;
            $client->phone = $request->phone;
            $client->email = $request->email;
            $client->save();
            $user = auth()->user();
            if ($user["id"] === 1) {
                    DB::table('third_relations')->where(['third_id' => $request->id])->delete();
                if (count($request->third_relations) > 0) {
                    for ($r = 0; $r < count($request->third_relations); $r++) {
                        DB::table('third_relations')->insert(['third_father' => $request->third_relations[$r]["value"], 'third_id' => $request->id]);
                    }
                }
            }
            return response()->json(["status" => 200, "client" => $client, "message" => "Client: " . $client->name . " " . $client->last_name . ". update"], 200);
        } else {
        return response()->json(["status" => 404, "client" => $client, "message" => "Client not found"], 404);
        }
    }

    public function clientInsert(Request $request) {
        $clientExist = Third::where([
            ["email", "=", $request->email],
        ])->first();
        if ($clientExist == null) {
            $client = Third::create(["name" => $request->name, "last_name" => $request->last_name, "phone" => $request->phone, "email" => $request->email, "third_type" => 2]);
            if ($client) {
                $user = auth()->user();
                DB::table('third_relations')->insert(['third_father' => $user["third_id"], 'third_id' => $client["id"]]);
                return response()->json(["status" => 200, "client" => $client, "message" => "Client: " . $request->name . " " . $request->last_name . ". add"], 200);
            } else {
                return response()->json(["status" => 204, "client" => $client, "message" => "Client not insert"], 204);
            }
        }
        return response()->json(["status" => 204, "client" => null, "message" => "Client email: " . $request->email . " exist"], 204);
    }

    public function clientDelete($id) {
        $client = Third::find($id);
        if ($client != null) {
            DB::table('third_relations')->where(['third_id' => $client["id"]])->delete();
            $client->delete();
            return response()->json(["status" => 200, "client" => $client, "message" => "Client: " . $client->name . " " . $client->last_name . ". delete"], 200);
        } else {
        return response()->json(["status" => 404, "client" => $client, "message" => "Client not found"], 404);
        }
    }

    public function clientOwners($id) {
        $thirds = Third::where([
            ['id', '!=', 1],
            ["third.third_type", "=", 1],
        ])->get();
        $thirdsSelected = DB::table('third_relations')->select('third.*')
                ->join('third', 'third.id', '=', "third_relations.third_father")
                ->where([
                    ["third_relations.third_id", "=", $id],
                ])->get();
        return response()->json(["status" => 200, "users" => $thirds, "usersSelected" => $thirdsSelected], 200);
    }
}
