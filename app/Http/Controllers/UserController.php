<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller
{

    private $status_code = 200;

    public function userLogin(Request $request) {

        $validator = Validator::make($request->all(),
            [
                "username" => "required",
                "password" => "required"
            ]
        );

        if($validator->fails()) {
            return response()->json(["status" => 401, "error" => $validator->errors()], 401);
        }

        $credentials = request(['username', 'password']);
        if (! $token = auth()->attempt($credentials)) {
            return response()->json(["status" => 401, 'message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function userLogout(Request $request) {
        auth()->logout();
        return response()->json(["status" => 200, 'message' => 'Successfully logged out'], 200);
    }

    public function userProfile(Request $request) {
        $authUser = auth()->user();
        $user = User::where("id", "=", $authUser["id"])->first();
        $user["roles"] =  User::getRoles($user["id"])->get();
        return response()->json(["status" => 200, "user" => $user], 200);
    }

    public function refresh() {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token) {
        return response()->json([
            'access_token' => $token,
            'expires_in' => auth()->factory()->getTTL() * 60,
            'status' => 200,
            'message' => 'Success login',
        ], 200);
    }
}
