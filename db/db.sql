-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: tvmarkets-test
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `admin` tinyint NOT NULL DEFAULT '0',
  `employee` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin',1,1),(2,'Employee',0,1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_relations`
--

DROP TABLE IF EXISTS `roles_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles_relations` (
  `role_id` bigint unsigned NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `fk_table1_users1_idx` (`user_id`),
  CONSTRAINT `fk_table1_roles1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `fk_table1_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_relations`
--

LOCK TABLES `roles_relations` WRITE;
/*!40000 ALTER TABLE `roles_relations` DISABLE KEYS */;
INSERT INTO `roles_relations` VALUES (1,1),(2,2),(2,3),(2,4);
/*!40000 ALTER TABLE `roles_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `third`
--

DROP TABLE IF EXISTS `third`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `third` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `third_type` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `third_email_uindex` (`email`),
  KEY `third_type_fk` (`third_type`),
  CONSTRAINT `third_type_fk` FOREIGN KEY (`third_type`) REFERENCES `third_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `third`
--

LOCK TABLES `third` WRITE;
/*!40000 ALTER TABLE `third` DISABLE KEYS */;
INSERT INTO `third` VALUES (1,'Super','Administrator','+000000000000','admin@tvmarkets.com',1),(2,'Daniel','Villanueva','+573163210300','villanueva.danielx@gmail.com',1),(3,'Joaquin','Del Olmo','+1666666666','jdolmo@tvmarkets.com',2),(4,'Emplado','#1','+234234234234','emplado1@tvmarkets.com',1),(5,'Emplado','#2','+000000000000','emplado2@tvmarkets.com',1),(6,'Sonia','Benitez','+000000000000','cliente1@tvmarkets.com',2),(7,'Cliente','#2','+000000000000','cliente2@tvmarkets.com',2),(8,'Cliente','# 3','+5165656565','cliente3@example.com',2),(9,'Clientessssss','#4','+58698745623','cliente4@example.com',2),(11,'Cliente Test Daniel','Cliente prueba','54554454545','cliente-daniel@ddd.com',2),(12,'Cliente 2 Daniel','Test','1545545554','ddddł@dddd.com',2),(18,'Wiliam','Vega','23423423423423423','wiliam.vega@example.com',1),(20,'ejemplo','empleado2','54234234234','ssss@ddddd.com',1);
/*!40000 ALTER TABLE `third` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `third_relations`
--

DROP TABLE IF EXISTS `third_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `third_relations` (
  `third_father` int NOT NULL,
  `third_id` int NOT NULL,
  KEY `third_relations_father_fk` (`third_father`),
  KEY `third_relations_third_id_fk` (`third_id`),
  CONSTRAINT `third_relations_father_fk` FOREIGN KEY (`third_father`) REFERENCES `third` (`id`),
  CONSTRAINT `third_relations_third_id_fk` FOREIGN KEY (`third_id`) REFERENCES `third` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `third_relations`
--

LOCK TABLES `third_relations` WRITE;
/*!40000 ALTER TABLE `third_relations` DISABLE KEYS */;
INSERT INTO `third_relations` VALUES (2,7),(2,11),(2,12),(2,3),(2,6),(4,6);
/*!40000 ALTER TABLE `third_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `third_type`
--

DROP TABLE IF EXISTS `third_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `third_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `third_type`
--

LOCK TABLES `third_type` WRITE;
/*!40000 ALTER TABLE `third_type` DISABLE KEYS */;
INSERT INTO `third_type` VALUES (1,'Employee'),(2,'Client');
/*!40000 ALTER TABLE `third_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `password` text,
  `third_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `users_third_fk` (`third_id`),
  CONSTRAINT `users_third_fk` FOREIGN KEY (`third_id`) REFERENCES `third` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','Administrator','$2y$10$9knBB0J3y/JlME4drA8OkO8.v40I3j8s4IGsck0RTbPohmEtHTlWi',1),(2,'danieljx','Daniel villanueva','$2y$10$9knBB0J3y/JlME4drA8OkO8.v40I3j8s4IGsck0RTbPohmEtHTlWi',2),(3,'empleado1','Emploado 1','$2y$10$9knBB0J3y/JlME4drA8OkO8.v40I3j8s4IGsck0RTbPohmEtHTlWi',4),(4,'empleado2','Empleado 2','$2y$10$9knBB0J3y/JlME4drA8OkO8.v40I3j8s4IGsck0RTbPohmEtHTlWi',5);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-23 17:11:25
