## Test Tvmarkets (Reactjs, Laravel 8.12, Mysql 8.0.23)

**.env**

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=tvmarkets-test
DB_USERNAME=tu usuario
DB_PASSWORD=tu clave

**DB Mysql**

_/tvmarkets-test/db/db.sql_

**Start project**

### 1.- Start Frontend

> npm run watch

### 2.- Start Backend

> php artisan serve
